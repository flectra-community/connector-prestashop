# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from flectra import _, models


class QueueJob(models.Model):
    _inherit = "queue.job"

    def related_action_record(self, binding_id_pos=0):
        self.ensure_one()

        binding_model = self.model_name
        binding_id = self.args[binding_id_pos]
        record = self.env[binding_model].browse(binding_id)
        flectra_name = record.flectra_id._name

        action = {
            "name": _(flectra_name),
            "type": "ir.actions.act_window",
            "res_model": flectra_name,
            "view_type": "form",
            "view_mode": "form",
            "res_id": record.flectra_id.id,
        }
        return action
