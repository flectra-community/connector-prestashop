# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from flectra import fields, models

from flectra.addons.component.core import Component


class ProductPricelist(models.Model):
    _inherit = "product.pricelist"

    prestashop_groups_bind_ids = fields.One2many(
        comodel_name="prestashop.groups.pricelist",
        inverse_name="flectra_id",
        string="PrestaShop user groups",
    )


class PrestashopGroupsPricelist(models.Model):
    _name = "prestashop.groups.pricelist"
    _inherit = "prestashop.binding.flectra"
    _inherits = {"product.pricelist": "flectra_id"}
    _description = "Group pricelist prestashop bindings"

    flectra_id = fields.Many2one(
        comodel_name="product.pricelist",
        required=True,
        ondelete="cascade",
        string="Flectra Pricelist",
    )


class PricelistAdapter(Component):
    _name = "prestashop.groups.pricelist.adapter"
    _inherit = "prestashop.adapter"
    _apply_on = "prestashop.groups.pricelist"
    _prestashop_model = "groups"
