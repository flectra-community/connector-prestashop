# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import logging

from flectra import fields, models

from flectra.addons.component.core import Component

_logger = logging.getLogger(__name__)


class PrestashopDeliveryCarrier(models.Model):
    _name = "prestashop.delivery.carrier"
    _inherit = "prestashop.binding.flectra"
    _inherits = {"delivery.carrier": "flectra_id"}
    _description = "PrestaShop Carrier"

    flectra_id = fields.Many2one(
        comodel_name="delivery.carrier",
        string="Delivery carrier",
        required=True,
        ondelete="cascade",
    )
    id_reference = fields.Integer(
        string="Reference ID",
        help="In PrestaShop, carriers can be copied with the same 'Reference "
        "ID' (only the last copied carrier will be synchronized with the "
        "ERP)",
    )
    name_ext = fields.Char(
        string="Name in PrestaShop",
    )
    active_ext = fields.Boolean(
        string="Active in PrestaShop",
    )
    export_tracking = fields.Boolean(
        string="Export tracking numbers to PrestaShop",
    )

    _sql_constraints = [
        (
            "prestashop_erp_uniq",
            "unique(backend_id, flectra_id, id_reference)",
            "An ERP record with same ID already exists on PrestaShop with the "
            "same id_reference",
        ),
    ]


class DeliveryCarrier(models.Model):
    _inherit = "delivery.carrier"

    prestashop_bind_ids = fields.One2many(
        comodel_name="prestashop.delivery.carrier",
        inverse_name="flectra_id",
        string="PrestaShop Bindings",
    )


class DeliveryCarrierAdapter(Component):
    _name = "prestashop.delivery.carrier.adapter"
    _inherit = "prestashop.adapter"
    _apply_on = "prestashop.delivery.carrier"

    _model_name = "prestashop.delivery.carrier"
    _prestashop_model = "carriers"

    def search(self, filters=None):
        if filters is None:
            filters = {}
        filters["filter[deleted]"] = 0
        return super().search(filters)
