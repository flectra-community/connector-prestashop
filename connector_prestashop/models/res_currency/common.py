# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from flectra import fields, models

from flectra.addons.component.core import Component


class PrestashopResCurrency(models.Model):
    _name = "prestashop.res.currency"
    _inherit = "prestashop.binding.flectra"
    _inherits = {"res.currency": "flectra_id"}
    _description = "Currency prestashop bindings"

    flectra_id = fields.Many2one(
        comodel_name="res.currency",
        string="Flectra Currency",
        required=True,
        ondelete="cascade",
    )


class ResCurrency(models.Model):
    _inherit = "res.currency"

    prestashop_bind_ids = fields.One2many(
        comodel_name="prestashop.res.currency",
        inverse_name="flectra_id",
        string="PrestaShop Bindings",
        readonly=True,
    )


class ResCurrencyAdapter(Component):
    _name = "prestashop.res.currency.adapter"
    _inherit = "prestashop.adapter"
    _apply_on = "prestashop.res.currency"
    _prestashop_model = "currencies"
