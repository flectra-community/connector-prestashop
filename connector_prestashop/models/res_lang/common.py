# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from flectra import fields, models

from flectra.addons.component.core import Component


class PrestashopResLang(models.Model):
    _name = "prestashop.res.lang"
    _inherit = "prestashop.binding.flectra"
    _inherits = {"res.lang": "flectra_id"}
    _description = "Shop lang prestashop bindings"

    flectra_id = fields.Many2one(
        comodel_name="res.lang",
        required=True,
        ondelete="cascade",
        string="Language",
    )
    active = fields.Boolean(
        string="Active in PrestaShop",
        default=False,
    )


class ResLang(models.Model):
    _inherit = "res.lang"

    prestashop_bind_ids = fields.One2many(
        comodel_name="prestashop.res.lang",
        inverse_name="flectra_id",
        readonly=True,
        string="PrestaShop Bindings",
    )


class ResLangAdapter(Component):
    _name = "prestashop.res.lang.adapter"
    _inherit = "prestashop.adapter"
    _apply_on = "prestashop.res.lang"
    _prestashop_model = "languages"
