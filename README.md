# Flectra Community / connector-prestashop

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[connector_prestashop_environment](connector_prestashop_environment/) | 2.0.1.0.0| Connector Prestashop Environment
[connector_prestashop](connector_prestashop/) | 2.0.2.0.2| PrestaShop-Odoo connector


