# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Connector Prestashop Environment",
    "version": "2.0.1.0.0",
    "author": "akretion,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/connector-prestashop",
    "category": "Connector",
    "depends": ["connector_prestashop", "server_environment"],
    "license": "AGPL-3",
    "installable": True,
}
